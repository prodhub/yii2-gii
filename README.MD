Yii2 Gii Generators
===================
For individual use only!

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist platx/yii2-gii "*"
```

or add

```
"platx/yii2-gii": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, add to your gii module config new generators like this :

```php
<?php 
'modules' => [
    ...
    'gii' => [
        'class' => 'yii\gii\Module',
        'generators' => [
            'papi' => 'platx\gii\generators\api\Generator',
            'pmodel' => 'platx\gii\generators\model\Generator',
            'pcrud' => 'platx\gii\generators\crud\Generator',
        ],
    ],
    ...    
]
?>```