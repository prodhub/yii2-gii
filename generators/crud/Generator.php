<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace platx\gii\generators\crud;

use kartik\grid\BooleanColumn;
use Yii;
use yii\db\ActiveRecord;
use yii\db\BaseActiveRecord;
use yii\gii\CodeFile;
use yii\helpers\ArrayHelper;
use yii\helpers\Inflector;
use yii\helpers\VarDumper;
use yii\web\Controller;

/**
 * Generates CRUD
 *
 * @property array $columnNames Model column names. This property is read-only.
 * @property string $controllerID The controller ID (without the module ID prefix). This property is
 * read-only.
 * @property array $searchAttributes Searchable attributes. This property is read-only.
 * @property boolean|\yii\db\TableSchema $tableSchema This property is read-only.
 * @property string $viewPath The controller view path. This property is read-only.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class Generator extends \yii\gii\Generator
{
    public $modelClass;
    public $controllerClass;
    public $extraActions = '';
    public $viewPath;
    public $baseControllerClass = 'backend\base\controllers\Backend';

    public $controllerActionIndexEnabled = true;
    public $controllerActionCreateEnabled = true;
    public $controllerActionUpdateEnabled = true;
    public $controllerActionDeleteEnabled = true;
    public $controllerActionApproveEnabled = false;
    public $controllerActionDisapproveEnabled = false;
    public $controllerActionDeleteSelectedEnabled = true;
    public $controllerActionApproveSelectedEnabled = false;
    public $controllerActionDisapproveSelectedEnabled = false;
    public $controllerActionDetailViewEnabled = false;

    public $headingTitle = 'Все записи';
    public $toolbarCreateButtonEnabled = true;
    public $toolbarApproveButtonEnabled = false;
    public $toolbarDisapproveButtonEnabled = false;
    public $toolbarDeleteButtonEnabled = true;
    public $toolbarRefreshButtonEnabled = true;
    public $checkboxColumnEnabled = true;
    public $actionColumnEnabled = true;
    public $actionApproveEnabled = false;
    public $actionDisapproveEnabled = false;
    public $idColumnEnabled = true;
    public $createdColumnEnabled = true;
    public $updatedColumnEnabled = false;
    public $detailColumnEnabled = false;
    public $pjax = false;
    public $condensed = true;
    public $hover = true;
    public $showPageSummary = true;
    public $resizableColumns = true;
    public $floatHeader = false;
    public $perfectScrollbar = false;
    public $toggleData = true;
    public $showHeader = true;
    public $bootstrap = true;
    public $bordered = true;
    public $striped = true;
    public $columns;

    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'Plat-X CRUD Generator';
    }

    /**
     * @inheritdoc
     */
    public function getDescription()
    {
        return 'This generator generates a controller and views that implement CRUD (Create, Read, Update, Delete)
            operations for the specified data model.';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [

            // For all scenarios
            [['modelClass'], 'required'],
            [['modelClass', 'baseControllerClass', 'extraActions'], 'filter', 'filter' => 'trim'],
            [['modelClass', 'controllerClass', 'baseControllerClass'], 'match', 'pattern' => '/^[\w\\\\]*$/', 'message' => 'Only word characters and backslashes are allowed.'],
            [['modelClass'], 'validateClass', 'params' => ['extends' => BaseActiveRecord::className()]],
            [['baseControllerClass'], 'validateClass', 'params' => ['extends' => Controller::className()]],
            [['controllerClass'], 'match', 'pattern' => '/Controller$/', 'message' => 'Controller class name must be suffixed with "Controller".'],
            [['controllerClass'], 'match', 'pattern' => '/(^|\\\\)[A-Z][^\\\\]+Controller$/', 'message' => 'Controller class name must start with an uppercase letter.'],
            [['controllerClass'], 'validateNewClass'],
            [['modelClass'], 'validateModelClass'],

            // Scenario controller
            [
                [
                    'controllerActionIndexEnabled', 'controllerActionCreateEnabled',
                    'controllerActionUpdateEnabled', 'controllerActionDeleteEnabled', 'controllerActionApproveEnabled',
                    'controllerActionDisapproveEnabled', 'controllerActionDeleteSelectedEnabled',
                    'controllerActionApproveSelectedEnabled', 'controllerActionDisapproveSelectedEnabled',
                    'controllerActionDetailViewEnabled',
                ], 'boolean', 'on' => 'controller'
            ],

            // Scenario index
            ['headingTitle', 'string', 'on' => 'index'],
            ['columns', 'safe', 'on' => 'index'],
            [[
                'toolbarCreateButtonEnabled', 'toolbarApproveButtonEnabled', 'toolbarDisapproveButtonEnabled',
                'toolbarDeleteButtonEnabled', 'toolbarRefreshButtonEnabled', 'checkboxColumnEnabled',
                'actionColumnEnabled', 'actionApproveEnabled', 'actionDisapproveEnabled',
                'idColumnEnabled', 'createdColumnEnabled', 'updatedColumnEnabled',
                'detailColumnEnabled', 'pjax', 'condensed',
                'hover', 'showPageSummary', 'resizableColumns',
                'floatHeader', 'perfectScrollbar', 'toggleData',
                'showHeader', 'bootstrap', 'bordered', 'striped'
            ], 'boolean', 'on' => 'index'],

            // Scenario update
            ['columns', 'safe', 'on' => 'index'],

            // Scenario detail
            ['columns', 'safe', 'on' => 'index'],
        ]);
    }

    public function scenarios()
    {
        return ArrayHelper::merge(parent::scenarios(), [
            'controller' => [
                'modelClass', 'baseControllerClass', 'controllerActionIndexEnabled', 'controllerActionCreateEnabled',
                'controllerActionUpdateEnabled', 'controllerActionDeleteEnabled', 'controllerActionApproveEnabled',
                'controllerActionDisapproveEnabled', 'controllerActionDeleteSelectedEnabled',
                'controllerActionApproveSelectedEnabled', 'controllerActionDisapproveSelectedEnabled',
                'controllerActionDetailViewEnabled',
            ],
            'index' => [
                'modelClass', 'toolbarCreateButtonEnabled', 'toolbarApproveButtonEnabled', 'toolbarDisapproveButtonEnabled',
                'toolbarDeleteButtonEnabled', 'toolbarRefreshButtonEnabled', 'checkboxColumnEnabled',
                'actionColumnEnabled', 'actionApproveEnabled', 'actionDisapproveEnabled',
                'idColumnEnabled', 'createdColumnEnabled', 'updatedColumnEnabled',
                'detailColumnEnabled', 'pjax', 'condensed',
                'hover', 'showPageSummary', 'resizableColumns',
                'floatHeader', 'perfectScrollbar', 'toggleData',
                'showHeader', 'bootstrap', 'bordered', 'striped', 'columns'
            ],
            'update' => [
                'modelClass', 'columns'
            ],
            'detail' => [
                'modelClass', 'columns'
            ],
        ]);
    }

    public function beforeValidate()
    {
        if(!$this->controllerClass) {
            $this->controllerClass = 'backend\\controllers\\' . $this->getModelName() . 'Controller';
        }

        return parent::beforeValidate();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'modelClass' => 'Model Class',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function hints()
    {
        return array_merge(parent::hints(), [
            'modelClass' => 'This is the ActiveRecord class associated with the table that CRUD will be built upon.
                You should provide a fully qualified class name, e.g., <code>common\models\Work</code>.',
            'pjax' => 'This indicates whether the generator should wrap the <code>GridView</code>
                widget on the index page with <code>yii\widgets\Pjax</code> widget. Set this to <code>true</code> if you want to get
                sorting, filtering and pagination without page refreshing.',
            'condensed' => 'Whether the grid table will have a `condensed` style. Applicable only if `bootstrap` is `true`.',
            'hover' => 'Whether the grid table will highlight row on `hover`. Applicable only if `bootstrap` is `true`.',
            'showPageSummary' => 'Whether to show the page summary row for the table. This will be displayed above the footer.',
            'resizableColumns' => 'Whether to allow resizing of columns',
            'floatHeader' => 'Whether the grid table will have a floating table header.',
            'perfectScrollbar' => 'Whether pretty perfect scrollbars using perfect scrollbar plugin is to be used.',
            'toggleData' => 'Whether to enable toggling of grid data. Defaults to `true`.',
            'showHeader' => 'Whether to show the header section of the grid table.',
            'bootstrap' => 'Whether the grid view will have Bootstrap table styling.',
            'bordered' => 'Whether the grid table will have a `bordered` style. Applicable only if `bootstrap` is `true`.',
            'striped' => 'Whether the grid table will have a `striped` style. Applicable only if `bootstrap` is `true`.',
            'extraActions' => 'Provide one or multiple action IDs to generate empty action method(s) in the controller. Separate multiple action IDs with commas or spaces.
                Action IDs should be in lower case. For example:
                <ul>
                    <li><code>index</code> generates <code>actionIndex()</code></li>
                    <li><code>create-order</code> generates <code>actionCreateOrder()</code></li>
                </ul>',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function requiredTemplates()
    {
        return ['controller.php'];
    }

    /**
     * @inheritdoc
     */
    public function stickyAttributes()
    {
        return array_merge(parent::stickyAttributes(), ['baseControllerClass']);
    }

    /**
     * Checks if model class is valid
     */
    public function validateModelClass()
    {
        /* @var $class ActiveRecord */
        $class = $this->modelClass;
        $pk = $class::primaryKey();
        if (empty($pk)) {
            $this->addError('modelClass', "The table associated with $class must have primary key(s).");
        }
    }

    /**
     * @inheritdoc
     */
    public function generate()
    {
        $files = [];

        $controllerFile = Yii::getAlias('@' . str_replace('\\', '/', ltrim($this->controllerClass, '\\')) . '.php');

        $files[] = new CodeFile($controllerFile, $this->render('controller.php'));

        $viewPath = $this->getViewPath();
        $templatePath = $this->getTemplatePath() . '/views';
        foreach (scandir($templatePath) as $file) {
            if (is_file($templatePath . '/' . $file) && pathinfo($file, PATHINFO_EXTENSION) === 'php') {
                $files[] = new CodeFile("$viewPath/$file", $this->render("views/$file"));
            }
        }
        foreach ($this->getExtraActions() as $extraAction) {
            $files[] = new CodeFile("$viewPath/$extraAction.php", $this->render("default-view.php", ['action' => $extraAction]));
        }

        return $files;
    }

    /**
     * @return string the controller ID (without the module ID prefix)
     */
    public function getControllerID()
    {
        $pos = strrpos($this->controllerClass, '\\');
        $class = substr(substr($this->controllerClass, $pos + 1), 0, -10);

        return Inflector::camel2id($class);
    }

    /**
     * @return string the controller view path
     */
    public function getViewPath()
    {
        if (empty($this->viewPath)) {
            return Yii::getAlias('@app/views/' . $this->getControllerID());
        } else {
            return Yii::getAlias($this->viewPath);
        }
    }

    /**
     * @return mixed
     */
    public function getModelName()
    {
        return end(explode('\\', $this->modelClass));
    }

    /**
     * @return mixed
     */
    public function getNameAttribute()
    {
        foreach ($this->getColumnNames() as $name) {
            if (!strcasecmp($name, 'name') || !strcasecmp($name, 'title')) {
                return $name;
            }
        }
        /* @var $class \yii\db\ActiveRecord */
        $class = $this->modelClass;
        $pk = $class::primaryKey();

        return $pk[0];
    }

    /**
     * Generates code for active field
     * @param string $attribute
     * @return string
     */
    public function generateActiveField($attribute, $isDisabled = false)
    {
        $tableSchema = $this->getTableSchema();
        if ($tableSchema === false || !isset($tableSchema->columns[$attribute])) {
            if (preg_match('/^(password|pass|passwd|passcode)$/i', $attribute)) {
                return "\$form->field(\$model, '$attribute')->passwordInput()";
            } else {
                return "\$form->field(\$model, '$attribute')";
            }
        }
        $column = $tableSchema->columns[$attribute];
        if ($column->phpType === 'boolean') {
            return "\$form->field(\$model, '$attribute')->checkbox()";
        } elseif ($column->type === 'text') {
            return "\$form->field(\$model, '$attribute')->textarea(['rows' => 6])";
        } else {
            if (preg_match('/^(password|pass|passwd|passcode)$/i', $column->name)) {
                $input = 'passwordInput';
            } else {
                $input = 'textInput';
            }
            if (is_array($column->enumValues) && count($column->enumValues) > 0) {
                $dropDownOptions = [];
                foreach ($column->enumValues as $enumValue) {
                    $dropDownOptions[$enumValue] = Inflector::humanize($enumValue);
                }
                return "\$form->field(\$model, '$attribute')->dropDownList("
                    . preg_replace("/\n\s*/", ' ', VarDumper::export($dropDownOptions)).", ['prompt' => ''])";
            } elseif ($column->phpType !== 'string' || $column->size === null) {
                $disabledText = $isDisabled ? '[\'disabled\' => true]' : '';
                return "\$form->field(\$model, '$attribute')->$input($disabledText)";
            } else {
                $disabledText = $isDisabled ? ', \'disabled\' => true]' : '';
                return "\$form->field(\$model, '$attribute')->$input(['maxlength' => true$disabledText])";
            }
        }
    }

    /**
     * Generates columns
     * @return array
     */
    public function generateColumns()
    {
        $tableSchema = $this->getTableSchema();

        if ($tableSchema === false) {
            return $this->getColumnNames();
        } else {
            $columns = [];

            foreach ($tableSchema->columns as $column) {
                if($this->checkColumn($column)) {
                    $columns[$column->name]['attribute'] = $column->name;
                    if ($column->type === 'boolean' || stripos($column->dbType, 'tinyint') !== false) {
                        $columns[$column->name]['class'] = BooleanColumn::className();
                        $columns[$column->name]['trueLabel'] = 'Да';
                        $columns[$column->name]['falseLabel'] = 'Нет';
                    }
                }
            }

            return $columns;
        }
    }

    /**
     * @return array
     */
    public function generateColumnsForDetailView()
    {
        $columns = $this->getColumnNames();
        $detailViewColumns = [];

        foreach ($columns as $column) {
            if (substr_count($column, 'content')) {
                $detailViewColumns[] = $column;
            }
            if (substr_count($column, 'image')) {
                $detailViewColumns[] = $column;
            }
            if (substr_count($column, 'icon')) {
                $detailViewColumns[] = $column;
            }
        }

        return $detailViewColumns;
    }

    /**
     * @param $column \yii\db\ColumnSchema
     * @return bool
     */
    public function checkColumn($column)
    {
        if($column->name == 'created_at' || $column->name == 'updated_at') {
            return false;
        }

        if($column->name == 'content_full' || $column->name == 'content_short' || $column->type == 'text') {
            return false;
        }

        if($column->name == 'id' && $this->idColumnEnabled) {
            return false;
        }

        if (substr_count($column->name, 'image') || substr_count($column->name, 'icon')) {
            return false;
        }

        return true;
    }

    /**
     * Generates action parameters
     * @return string
     */
    public function generateActionParams()
    {
        /* @var $class ActiveRecord */
        $class = $this->modelClass;
        $pks = $class::primaryKey();
        if (count($pks) === 1) {
            return '$id';
        } else {
            return '$' . implode(', $', $pks);
        }
    }

    /**
     * Generates parameter tags for phpdoc
     * @return array parameter tags for phpdoc
     */
    public function generateActionParamComments()
    {
        /* @var $class ActiveRecord */
        $class = $this->modelClass;
        $pks = $class::primaryKey();
        if (($table = $this->getTableSchema()) === false) {
            $params = [];
            foreach ($pks as $pk) {
                $params[] = '@param ' . (substr(strtolower($pk), -2) == 'id' ? 'integer' : 'string') . ' $' . $pk;
            }

            return $params;
        }
        if (count($pks) === 1) {
            return ['@param ' . $table->columns[$pks[0]]->phpType . ' $id'];
        } else {
            $params = [];
            foreach ($pks as $pk) {
                $params[] = '@param ' . $table->columns[$pk]->phpType . ' $' . $pk;
            }

            return $params;
        }
    }

    /**
     * Returns table schema for current model class or false if it is not an active record
     * @return boolean|\yii\db\TableSchema
     */
    public function getTableSchema()
    {
        /* @var $class ActiveRecord */
        $class = $this->modelClass;
        if (is_subclass_of($class, 'yii\db\ActiveRecord')) {
            return $class::getTableSchema();
        } else {
            return false;
        }
    }

    /**
     * @return array model column names
     */
    public function getColumnNames()
    {
        /* @var $class ActiveRecord */
        $class = $this->modelClass;
        if (is_subclass_of($class, 'yii\db\ActiveRecord')) {
            return $class::getTableSchema()->getColumnNames();
        } else {
            /* @var $model \yii\base\Model */
            $model = new $class();

            return $model->attributes();
        }
    }

    /**
     * @return array
     */
    public function generateColumnsForForm()
    {
        $columns = $this->getColumnNames();
        $mainColumns = [];
        $additionalColumns = [];

        foreach ($columns as $column) {
            if(substr_count($column, 'created') || substr_count($column, 'updated') || substr_count($column, 'enabled')) {
                $additionalColumns[] = $column;
            } else {
                $mainColumns[] = $column;
            }
        }

        return ['main' => $mainColumns, 'additional' => $additionalColumns];
    }

    /**
     * Normalizes [[extraActions]] into an array of additional actions.
     * @return array an array actions entered by the user
     */
    public function getExtraActions()
    {
        $extraActions = array_unique(preg_split('/[\s,]+/', $this->extraActions, -1, PREG_SPLIT_NO_EMPTY));
        sort($extraActions);

        return $extraActions;
    }
}
