<?php
/**
 * This is the template for generating a CRUD controller class file.
 */

use yii\db\ActiveRecordInterface;
use yii\helpers\Inflector;
use yii\helpers\StringHelper;


/* @var $this yii\web\View */
/* @var $generator platx\gii\generators\crud\Generator */

echo "<?php\n";
?>

namespace <?= StringHelper::dirname(ltrim($generator->controllerClass, '\\')) ?>;

use <?= ltrim($generator->baseControllerClass, '\\') ?>;
use Yii;


/**
 * <?= $generator->generateString(Inflector::pluralize(Inflector::camel2words(StringHelper::basename($generator->modelClass)))) ?><?= "\n" ?>
 * @package backend\controllers
 */
class WorkController extends Backend
{
    /** @var string Model class for CRUD */
    public $modelClass = '<?= $generator->modelClass ?>';
<?php foreach ($generator->getExtraActions() as $action): ?>

    /**
    * @return string
    */
    public function action<?= Inflector::id2camel($action) ?>()
    {
        return $this->render('<?= $action ?>', []);
    }
<?php endforeach; ?>
<?php if(!$generator->controllerActionIndexEnabled) : ?>

    /**
    * Index
    * @return string
    */
    public function actionIndex()
    {
        throw new \yii\web\MethodNotAllowedHttpException();
    }
<?php endif; ?>
<?php if(!$generator->controllerActionCreateEnabled) : ?>

    /**
    * Create
    * @return string
    */
    public function actionCreate()
    {
        throw new \yii\web\MethodNotAllowedHttpException();
    }
<?php endif; ?>
<?php if(!$generator->controllerActionUpdateEnabled) : ?>

    /**
    * Update
    * @param $id
    * @return string
    */
    public function actionUpdate($id)
    {
        throw new \yii\web\MethodNotAllowedHttpException();
    }
<?php endif; ?>
<?php if(!$generator->controllerActionDeleteEnabled) : ?>

    /**
    * Delete
    * @param $id
    * @throws ErrorException
    */
    public function actionDelete($id)
    {
        throw new \yii\web\MethodNotAllowedHttpException();
    }
<?php endif; ?>
<?php if(!$generator->controllerActionApproveEnabled) : ?>

    /**
    * Approve
    * @param $id
    * @throws ErrorException
    */
    public function actionApprove($id)
    {
        throw new \yii\web\MethodNotAllowedHttpException();
    }
<?php endif; ?>
<?php if(!$generator->controllerActionDisapproveEnabled) : ?>

    /**
    * Disapprove
    * @param $id
    * @throws ErrorException
    */
    public function actionDisapprove($id)
    {
        throw new \yii\web\MethodNotAllowedHttpException();
    }
<?php endif; ?>
<?php if(!$generator->controllerActionDeleteSelectedEnabled) : ?>

    /**
    * Delete selected items
    * @return int
    */
    public function actionDeleteSelected()
    {
        throw new \yii\web\MethodNotAllowedHttpException();
    }
<?php endif; ?>
<?php if(!$generator->controllerActionApproveSelectedEnabled) : ?>

    /**
    * Approve selected items
    * @return int
    */
    public function actionApproveSelected()
    {
        throw new \yii\web\MethodNotAllowedHttpException();
    }
<?php endif; ?>
<?php if(!$generator->controllerActionDisapproveSelectedEnabled) : ?>

    /**
    * Disapprove selected items
    * @return int
    */
    public function actionDisapproveSelected()
    {
        throw new \yii\web\MethodNotAllowedHttpException();
    }
<?php endif; ?>
<?php if(!$generator->controllerActionDetailViewEnabled) : ?>

    /**
    * Detail view in grid
    * @return int
    */
    public function actionDetailView()
    {
        throw new \yii\web\MethodNotAllowedHttpException();
    }
<?php endif; ?>
}