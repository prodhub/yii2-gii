<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */
/* @var $action string */

echo "<?php\n";
?>

/**
* @var yii\web\View $this
*/

$this->title = <?= $generator->generateString('Action ' . Inflector::camel2words(StringHelper::basename($action))) ?>;
$this->params['breadcrumbs'][] = ['label' => <?= $generator->generateString(Inflector::pluralize(Inflector::camel2words(StringHelper::basename($generator->modelClass)))) ?>, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-lg-12">
        <div class="box box-primary">
            <?= 'Action ' . Inflector::camel2words(StringHelper::basename($action)) ?><?= "\n" ?>
        </div>
    </div>
</div>
