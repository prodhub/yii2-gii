<?php

/* @var $this yii\web\View */
/* @var $generator platx\gii\generators\crud\Generator */

$columns = $generator->generateColumnsForDetailView();

echo "<?php\n";
?>

/* @var $this yii\web\View */
<?= !empty($generator->modelClass) ? "/* @var \$model " . ltrim($generator->modelClass, '\\') . " */\n" : '' ?>

?>
<div class="row">
    <div class="col-lg-6">
<?php foreach ($columns as $column) : ?>
        <div><?= "<?= " ?>$model-><?= $column ?> ?></div>
<?php endforeach; ?>
    </div>
</div>
