<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator platx\gii\generators\crud\Generator */

$nameAttribute = $generator->getNameAttribute();

$columns = $generator->generateColumns();

echo "<?php\n";
?>

use yii\helpers\Html;
use common\widgets\\DynaGrid;

/* @var $this yii\web\View */
<?= !empty($generator->modelClass) ? "/* @var \$model " . ltrim($generator->modelClass, '\\') . " */\n" : '' ?>
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = <?= $generator->generateString(Inflector::pluralize(Inflector::camel2words(StringHelper::basename($generator->modelClass)))) ?>;
$this->params['breadcrumbs'][] = $this->title;
?>

<?php echo "<?php\n"; ?><?= "\n" ?>
$columns = [
<?php foreach ($columns as $column) : ?>
    [
<?php foreach ($column as $key => $value) : ?>
        '<?= $key ?>' => '<?= $value ?>',
<?php endforeach; ?>
    ],
<?php endforeach; ?>
];
?>

<?= "<?= " ?>DynaGrid::widget([
    'gridId' => 'dynagrid-<?= $generator->getControllerID() ?>',
    'columns' => $columns,
    'gridOptions' => [
        'dataProvider' => $dataProvider,
        'filterModel' => $model,
        'headingTitle' => '<?= $generator->headingTitle ?>',
        'idColumnEnabled' => <?= $generator->idColumnEnabled ? 'true' : 'false' ?>,
        'checkboxColumnEnabled' => <?= $generator->checkboxColumnEnabled ? 'true' : 'false' ?>,
        'detailColumnEnabled' => <?= $generator->detailColumnEnabled ? 'true' : 'false' ?>,
        'toolbarCreateButtonEnabled' => <?= $generator->toolbarCreateButtonEnabled ? 'true' : 'false' ?>,
        'toolbarDeleteButtonEnabled' => <?= $generator->toolbarDeleteButtonEnabled ? 'true' : 'false' ?>,
        'toolbarApproveButtonEnabled' => <?= $generator->toolbarApproveButtonEnabled ? 'true' : 'false' ?>,
        'toolbarDisapproveButtonEnabled' => <?= $generator->toolbarDisapproveButtonEnabled ? 'true' : 'false' ?>,
        'toolbarRefreshButtonEnabled' => <?= $generator->toolbarRefreshButtonEnabled ? 'true' : 'false' ?>,
        'actionApproveEnabled' => <?= $generator->actionApproveEnabled ? 'true' : 'false' ?>,
        'actionDisapproveEnabled' => <?= $generator->actionDisapproveEnabled ? 'true' : 'false' ?>,
        'createdColumnEnabled' => <?= $generator->createdColumnEnabled ? 'true' : 'false' ?>,
        'updatedColumnEnabled' => <?= $generator->updatedColumnEnabled ? 'true' : 'false' ?>,
        'actionColumnEnabled' => <?= $generator->actionColumnEnabled ? 'true' : 'false' ?>,
        'pjax' => <?= $generator->pjax ? 'true' : 'false' ?>,
        'condensed' => <?= $generator->condensed ? 'true' : 'false' ?>,
        'hover' => <?= $generator->hover ? 'true' : 'false' ?>,
        'showPageSummary' => <?= $generator->showPageSummary ? 'true' : 'false' ?>,
        'resizableColumns' => <?= $generator->resizableColumns ? 'true' : 'false' ?>,
        'floatHeader' => <?= $generator->floatHeader ? 'true' : 'false' ?>,
        'perfectScrollbar' => <?= $generator->perfectScrollbar ? 'true' : 'false' ?>,
        'toggleData' => <?= $generator->toggleData ? 'true' : 'false' ?>,
        'showHeader' => <?= $generator->showHeader ? 'true' : 'false' ?>,
        'bootstrap' => <?= $generator->bootstrap ? 'true' : 'false' ?>,
        'bordered' => <?= $generator->bordered ? 'true' : 'false' ?>,
        'striped' => <?= $generator->striped ? 'true' : 'false' ?>,
    ]
]) ?>