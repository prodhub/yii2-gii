<?php

/* @var $this yii\web\View */
/* @var $generator platx\gii\generators\crud\Generator */
/* @var $model \yii\db\ActiveRecord */


$model = new $generator->modelClass();
$safeAttributes = $model->safeAttributes();
if (empty($safeAttributes)) {
    $safeAttributes = $model->attributes();
}

$columns = $generator->generateColumnsForForm();

echo "<?php\n";
?>

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model <?= ltrim($generator->modelClass, '\\') ?> */
/* @var $form yii\widgets\ActiveForm */
?>

<?= "<?php " ?>$form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <div class="box-body">
<?php if(!empty($columns['main'])) : ?>
<?php foreach ($columns['main'] as $attribute) : ?>
<?php if (in_array($attribute, $safeAttributes)) : ?>
       <?= "<?= " ?><?= $generator->generateActiveField($attribute) ?> ?> <?= "\n" ?>
<?php endif; ?>
<?php endforeach; ?>
<?php endif; ?>
<?php if(!empty($columns['additional'])) : ?>

       <?= '<?php ' ?>if(!$model->isNewRecord) : ?>
<?php foreach ($columns['additional'] as $attribute) : ?>
<?php if (in_array($attribute, $safeAttributes)) : ?>
            <?= "<?= " ?><?= $generator->generateActiveField($attribute, true) ?> ?> <?= "\n" ?>
<?php endif; ?>
<?php endforeach; ?>
       <?= '<?php ' ?>endif; ?>
<?php endif; ?>
    </div>
    <div class="box-footer">
        <?= "<?= " ?>Html::submitButton($model->isNewRecord ? <?= $generator->generateString('Создать') ?> : <?= $generator->generateString('Изменить') ?>, ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
<?= "<?php " ?>ActiveForm::end(); ?>
