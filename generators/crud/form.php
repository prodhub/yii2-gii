<?php
/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $generator yii\gii\generators\crud\Generator */

?>
<?= $form->field($generator, 'modelClass') ?>

<legend>Controller functionality</legend>
<?= $form->field($generator, 'controllerActionIndexEnabled')->checkbox() ?>
<?= $form->field($generator, 'controllerActionCreateEnabled')->checkbox() ?>
<?= $form->field($generator, 'controllerActionUpdateEnabled')->checkbox() ?>
<?= $form->field($generator, 'controllerActionDeleteEnabled')->checkbox() ?>
<?= $form->field($generator, 'controllerActionDeleteSelectedEnabled')->checkbox() ?>
<?= $form->field($generator, 'controllerActionApproveEnabled')->checkbox() ?>
<?= $form->field($generator, 'controllerActionApproveSelectedEnabled')->checkbox() ?>
<?= $form->field($generator, 'controllerActionDisapproveEnabled')->checkbox() ?>
<?= $form->field($generator, 'controllerActionDisapproveSelectedEnabled')->checkbox() ?>
<?= $form->field($generator, 'controllerActionDetailViewEnabled')->checkbox() ?>
<?= $form->field($generator, 'extraActions') ?>

<div class="row">
    <div class="col-lg-6">
        <legend>Grid functionality</legend>
        <?= $form->field($generator, 'headingTitle') ?>
        <?= $form->field($generator, 'toolbarCreateButtonEnabled')->checkbox() ?>
        <?= $form->field($generator, 'toolbarApproveButtonEnabled')->checkbox() ?>
        <?= $form->field($generator, 'toolbarDisapproveButtonEnabled')->checkbox() ?>
        <?= $form->field($generator, 'toolbarDeleteButtonEnabled')->checkbox() ?>
        <?= $form->field($generator, 'toolbarRefreshButtonEnabled')->checkbox() ?>
        <?= $form->field($generator, 'checkboxColumnEnabled')->checkbox() ?>
        <?= $form->field($generator, 'actionColumnEnabled')->checkbox() ?>
        <?= $form->field($generator, 'actionApproveEnabled')->checkbox() ?>
        <?= $form->field($generator, 'actionDisapproveEnabled')->checkbox() ?>
        <?= $form->field($generator, 'idColumnEnabled')->checkbox() ?>
        <?= $form->field($generator, 'createdColumnEnabled')->checkbox() ?>
        <?= $form->field($generator, 'updatedColumnEnabled')->checkbox() ?>
        <?= $form->field($generator, 'detailColumnEnabled')->checkbox() ?>
    </div>
    <div class="col-lg-6">
        <legend>Grid style</legend>
        <?= $form->field($generator, 'pjax')->checkbox() ?>
        <?= $form->field($generator, 'condensed')->checkbox() ?>
        <?= $form->field($generator, 'hover')->checkbox() ?>
        <?= $form->field($generator, 'showPageSummary')->checkbox() ?>
        <?= $form->field($generator, 'resizableColumns')->checkbox() ?>
        <?= $form->field($generator, 'floatHeader')->checkbox() ?>
        <?= $form->field($generator, 'perfectScrollbar')->checkbox() ?>
        <?= $form->field($generator, 'toggleData')->checkbox() ?>
        <?= $form->field($generator, 'showHeader')->checkbox() ?>
        <?= $form->field($generator, 'bootstrap')->checkbox() ?>
        <?= $form->field($generator, 'bordered')->checkbox() ?>
        <?= $form->field($generator, 'striped')->checkbox() ?>
    </div>
</div>



