<?php
/**
 * This is the template for generating the controller class for api module.
 */
use yii\helpers\Inflector;

/* @var $this yii\web\View */
/* @var $generator platx\gii\generators\api\Generator */

echo "<?php\n";
?>

namespace <?= $generator->generateControllerNamespace?>;

/**
 * This is controller class for model <?= $generator->baseModelClass ?><?= "\n" ?>
 * @package <?= $generator->generateControllerNamespace ?><?= "\n" ?>
 */
class <?= $generator->getBaseModelName() ?>Controller extends <?= '\\' . ltrim($generator->baseControllerClass, '\\') . "\n" ?>
{
    /**
     * @var string
     */
    public $modelClass = '<?= $generator->baseModelClass ?>';

    /**
     * @var string
     */
    public $searchFormClass = <?= $generator->searchFormName ? "'{$generator->generateSearchFormNamespace}\\{$generator->searchFormName}'" : false ?><?= "\n" ?>

    /**
     * @return array
     */
    public function actions()
    {
        return [
            <?php if($generator->generateActionIndex) : ?><?= "\n" ?>
                'index' => [
                    'class' => '<?= $generator->baseActionIndexClass ?>',
                    'searchFormClass' => $this->searchFormClass,
                ],
            <?php endif; ?>
            <?php if($generator->generateActionIndex) : ?><?= "\n" ?>
                'view' => [
                    'class' => '<?= $generator->baseActionViewClass ?>',
                    'modelClass' => $this->modelClass,
                ],
            <?php endif; ?>
            <?php if($generator->generateActionCreate) : ?><?= "\n" ?>
                'create' => [
                    'class' => '<?= $generator->baseActionCreateClass ?>',
                    'modelClass' => $this->modelClass,
                ],
            <?php endif; ?>
            <?php if($generator->generateActionUpdate) : ?><?= "\n" ?>
                'update' => [
                    'class' => '<?= $generator->baseActionUpdateClass ?>',
                    'modelClass' => $this->modelClass,
                ],
            <?php endif; ?>
            <?php if($generator->generateActionDelete) : ?><?= "\n" ?>
                'delete' => [
                    'class' => '<?= $generator->baseActionDeleteClass ?>',
                    'modelClass' => $this->modelClass,
                ],
            <?php endif; ?><?= "\n" ?>
        ];
    }

<?php foreach ($generator->getExtraActions() as $action): ?>
    /**
     * @return string
     */
    public function action<?= Inflector::id2camel($action) ?>()
    {
        return '<?= $action ?>';
    }
<?php endforeach; ?>
}