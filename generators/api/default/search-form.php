<?php
/**
 * This is the template for generating the model class for api module.
 */

/* @var $this yii\web\View */
/* @var $generator platx\gii\generators\api\Generator */

echo "<?php\n";
?>

namespace <?= $generator->generateSearchFormNamespace ?>;

use yii\helpers\ArrayHelper;


/**
 * This is search form class for model <?= $generator->baseModelClass . "\n" ?>
 * @package <?= $generator->generateSearchFormNamespace . "\n" ?>
 */
class <?= $generator->getBaseModelName() ?><?= $generator->searchFormClassSuffix ?> extends <?= '\\' . ltrim($generator->baseSearchFormClass, '\\') . "\n" ?>
{
    /**
     * @var string
     */
    public $modelClass = '<?= $generator->baseModelClass ?>';

    /**
     * @return array
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [

        ]);
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [

        ]);
    }

    /**
     *
     */
    protected function filterAttributes()
    {
        parent::filterAttributes();
    }
}