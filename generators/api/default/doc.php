<?php
/**
 * This is the template for generating documentation for api method.
 */

/* @var $this yii\web\View */
/* @var $generator platx\gii\generators\api\Generator */
$baseModel = $generator->baseModel;
$generatedModel = $generator->getGeneratedModel();

if(!$generatedModel) {
   $generatedModel = $baseModel;
}

$expand = implode(',', array_keys($generatedModel->extraFields()));
echo "<?php\n";
?>

<?php if($generator->generateActionIndex) : ?>
/**
 * @api {get} /api/<?= $generator->getControllerId() ?> Список <?= $generator->getBaseModelName() . "\n"?>
 * @apiName Index
 * @apiGroup <?= $generator->getBaseModelName() . "\n" ?>
 * @apiPermission Всем
 *
 * @apiUse IndexDefaultParams
<?php if($expand) : ?>
 * @apiParam (Атрибуты запроса) {String=<?= implode(',', array_keys($generatedModel->extraFields())) ?>} [expand] Дополнительные поля (<u>Через запятую</u>) <?= "\n" ?>
<?php endif; ?>
 *
 * @apiUse IndexDefaultSuccess
 * @apiSuccess (Атрибуты ответа) {Object[]} items Массив записей
<?php foreach ($generatedModel->getTableSchema()->getColumnNames() as $field) : ?>
 * @apiSuccess (Атрибуты ответа) {<?= $generatedModel->getTableSchema()->getColumn($field)->phpType ?>} items.<?= $field ?> <?= $generatedModel->getAttributeLabel($field) ?> <?= "\n" ?>
<?php endforeach; ?> <?= "*\n" ?>
 * @apiUse Error
 *
 * @apiSuccessExample {json} По умолчанию:
 *      HTTP/1.1 200 OK
 *      {
 *          count_all: 20,
 *          count_current: 10,
 *          limit: 10,
 *          offset: 0,
 *          items: [
 *              0: {
 *
 *              }
 *          ]
 *      }
 *
 * @apiUse ErrorNotAllowedGet
 */

<?php endif; ?>
<?php if($generator->generateActionView) : ?>
/**
 * @api {get} /api/<?= $generator->getControllerId() ?>/:id Просмотр <?= $generator->getBaseModelName() . "\n" ?>
 * @apiName View
 * @apiGroup <?= $generator->getBaseModelName() . "\n" ?>
 * @apiPermission Всем
 * @apiExample {get} С параметром id:
 *      /api/<?= $generator->getControllerId() ?>/1
 *
 * @apiParam (Атрибуты запроса) {String} [fields="Все"] Какие поля выбирать (<u>Через запятую</u>)
<?php if($expand) : ?>
 * @apiParam (Атрибуты запроса) {String=<?= implode(',', array_keys($generatedModel->extraFields())) ?>} [expand] Дополнительные поля (<u>Через запятую</u>) <?= "\n" ?>
<?php endif; ?>
 *
<?php foreach ($generatedModel->getTableSchema()->getColumnNames() as $field) : ?>
 * @apiSuccess (Атрибуты ответа) {<?= $generatedModel->getTableSchema()->getColumn($field)->phpType ?>} <?= $field ?> <?= $generatedModel->getAttributeLabel($field) ?> <?= "\n" ?>
<?php endforeach; ?> <?= "*\n" ?>
 *
 * @apiUse Error
 *
 * @apiSuccessExample {json} Стандартно:
 *      HTTP/1.1 200 OK
 *      {
 *
 *      }
 *
 * @apiUse ErrorNotFound
 * @apiUse ErrorNotAllowedGet
 */

<?php endif; ?>
<?php if($generator->generateActionCreate) : ?>
/**
 * @api {post} /api/<?= $generator->getControllerId() ?> Создание <?= $generator->getBaseModelName() . "\n" ?>
 * @apiName Create
 * @apiGroup <?= $generator->getBaseModelName() . "\n" ?>
 * @apiPermission Всем
 *
<?php foreach ($generatedModel->safeAttributes() as $safeAttribute) : ?>
<?php if ($column = $generatedModel->getTableSchema()->getColumn($safeAttribute)) : ?>
 * @apiParam (Атрибуты запроса) {<?= $column->phpType ?>} <?= $safeAttribute ?> <?= $generatedModel->getAttributeLabel($safeAttribute) ?> <?= "\n" ?>
<?php endif; ?>
<?php endforeach; ?> <?= "*\n" ?>
<?php if($expand) : ?>
 * @apiParam (Атрибуты запроса) {String=<?= implode(',', array_keys($generatedModel->extraFields())) ?>} [expand] Дополнительные поля (<u>Через запятую</u>) <?= "\n" ?>
<?php endif; ?>
 *
<?php foreach ($generatedModel->getTableSchema()->getColumnNames() as $field) : ?>
 * @apiSuccess (Атрибуты ответа) {<?= $generatedModel->getTableSchema()->getColumn($field)->phpType ?>} <?= $field ?> <?= $generatedModel->getAttributeLabel($field) ?> <?= "\n" ?>
<?php endforeach; ?> <?= "*\n" ?>
 *
 * @apiUse Error
 *
 * @apiSuccessExample {json} По умолчанию:
 *      HTTP/1.1 200 OK
 *      {
 *
 *      }
 *
 * @apiUse ErrorValidationFailed
 * @apiUse ErrorNotAllowedPost
 */

<?php endif; ?>