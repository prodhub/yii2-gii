<?php
/**
 * This is the template for generating the model class for api module.
 */

/* @var $this yii\web\View */
/* @var $generator platx\gii\generators\api\Generator */

echo "<?php\n";
?>


namespace <?= $generator->generateModelNamespace ?>;

use yii\helpers\ArrayHelper;


/**
* This is api model class for model "<?= $generator->baseModelClass ?>".
*
 * @package <?= $generator->generateModelNamespace . "\n" ?>
 */
class <?= $generator->getBaseModelName() ?> extends <?= '\\' . ltrim($generator->baseModelClass, '\\') . "\n" ?>
{
    /**
     * @return array
     */
    public function fields()
    {
        $items = parent::fields();

        return $items;
    }

    /**
    * @return array
    */
    public function excludeFields()
    {
        return ArrayHelper::merge(parent::excludeFields, [
<?php foreach($generator->getExcludeFieldsArray() as $excludeField) : ?>
            '<?= $excludeField ?>',
<?php endforeach; ?> <?= "\n" ?>
        ]);
    }

    /**
     * @return array
     */
    public function extraFields()
    {
        $items = parent::extraFields();

        return $items;
    }
}