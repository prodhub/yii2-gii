<?php

namespace platx\gii\generators\api;

use Yii;
use yii\base\Model;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Connection;
use yii\db\Schema;
use yii\gii\CodeFile;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Inflector;
use yii\base\NotSupportedException;

/**
 * This generator will generate api module.
 * @property string|false searchFormName
 * @property string generateModelClass
 * @property ActiveRecord|Model baseModel
 */
class Generator extends \yii\gii\Generator
{
    public $baseModelClass = '';
    public $excludeFields = '';
    public $extraActions = '';

    public $generateController = true;
    public $generateActionIndex = true;
    public $generateActionView = true;
    public $generateActionCreate = false;
    public $generateActionUpdate = false;
    public $generateActionDelete = false;
    public $generateModel = true;
    public $generateSearchForm = true;
    public $generateDoc = true;

    public $generateControllerNamespace = 'frontend\modules\api\controllers';
    public $generateModelNamespace = 'frontend\modules\api\models';
    public $generateSearchFormNamespace = 'frontend\modules\api\forms';
    public $generateDocFolder = 'frontend\modules\api\controllers\doc';

    public $searchFormClassSuffix = 'SearchForm';

    public $baseControllerClass = 'platx\rest\Controller';
    public $baseSearchFormClass = 'platx\rest\SearchForm';
    public $baseActionIndexClass = 'platx\rest\actions\IndexAction';
    public $baseActionCreateClass = 'platx\rest\actions\CreateAction';
    public $baseActionViewClass = 'platx\rest\actions\ViewAction';
    public $baseActionUpdateClass = 'platx\rest\actions\UpdateAction';
    public $baseActionDeleteClass = 'platx\rest\actions\DeleteAction';

    private $_baseModelName;
    private $_generateModelClass;
    private $_controllerId;
    private $_baseModel;
    private $_generatedModel;

    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'Plat-X Api Generator';
    }

    /**
     * @inheritdoc
     */
    public function getDescription()
    {
        return 'This generator generates code for api module.';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [[
                'baseModelClass',
            ], 'required'],
            [[
                'baseModelClass', 'generateControllerNamespace', 'generateSearchFormNamespace', 'generateDocFolder', 'generateModelNamespace',
                'baseControllerClass', 'baseActionIndexClass', 'baseActionCreateClass', 'baseActionViewClass',
                'baseActionUpdateClass', 'baseActionDeleteClass', 'baseSearchFormClass', 'excludeFields', 'extraActions'
            ], 'filter', 'filter' => 'trim'],

            [[
                'baseModelClass', 'generateControllerNamespace', 'generateSearchFormNamespace', 'generateDocFolder', 'generateModelNamespace',
                'baseControllerClass', 'baseActionIndexClass', 'baseActionCreateClass', 'baseActionViewClass',
                'baseActionUpdateClass', 'baseActionDeleteClass', 'baseSearchFormClass'
            ], 'filter', 'filter' => function($value) { return trim($value, '\\'); }],
            [[
                'baseModelClass', 'generateControllerNamespace', 'generateSearchFormNamespace', 'generateDocFolder', 'generateModelNamespace',
                'baseControllerClass', 'baseActionIndexClass', 'baseActionCreateClass', 'baseActionViewClass',
                'baseActionUpdateClass', 'baseActionDeleteClass', 'baseSearchFormClass'
            ], 'match', 'pattern' => '/^[\w\\\\]+$/', 'message' => 'Only word characters and backslashes are allowed.'],
            [[
                'generateController', 'generateActionIndex', 'generateActionView',
                'generateActionCreate', 'generateActionUpdate', 'generateActionDelete',
                'generateModel', 'generateSearchForm', 'generateDoc'
            ], 'boolean'],
            ['searchFormClassSuffix', 'required', 'when' => function($model){
                return !$model->generateSearchForm;
            }],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'baseModelClass' => 'Model class',
            'generateController' => 'Generate Controller',
            'generateModel' => 'Generate Model',
            'generateSearchForm' => 'Generate Search Form',
            'generateDoc' => 'Generate Doc',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function hints()
    {
        return [
            'excludeFields' => 'Provide one or multiple field names to exclude from model attributes. Separate multiple names with commas or spaces.',
            'baseModelClass' => 'Model class to generate API group.',
            'generateModel' => "This indicates whether the generator should generate ActiveRecord model for api module.",
            'generateController' => "This indicates whether the generator should generate API controller for api group.<br><code>$this->baseControllerClass</code>",
            'generateSearchForm' => "This indicates whether the generator should generate search form class for filtering data in api module.<br><code>$this->baseSearchFormClass</code>",
            'generateDoc' => 'This indicates whether the generator should generate documentation for this api group.',
            'extraActions' => 'Provide one or multiple action IDs to generate empty action method(s) in the controller. Separate multiple action IDs with commas or spaces.
                Action IDs should be in lower case. For example:
                <ul>
                    <li><code>index</code> generates <code>actionIndex()</code></li>
                    <li><code>create-order</code> generates <code>actionCreateOrder()</code></li>
                </ul>',
            'generateActionIndex' => "<code>$this->baseActionIndexClass</code>",
            'generateActionView' => "<code>$this->baseActionViewClass</code>",
            'generateActionCreate' => "<code>$this->baseActionCreateClass</code>",
            'generateActionUpdate' => "<code>$this->baseActionUpdateClass</code>",
            'generateActionDelete' => "<code>$this->baseActionDeleteClass</code>",
        ];
    }

    /**
     * @inheritdoc
     */
    public function requiredTemplates()
    {
        // @todo make 'query.php' to be required before 2.1 release
        return ['model.php'/*, 'query.php'*/];
    }

    /**
     * @inheritdoc
     */
    public function stickyAttributes()
    {
        return array_merge(parent::stickyAttributes(), ['generateController', 'generateModel', 'generateDoc', 'generateSearchForm']);
    }

    /**
     * @inheritdoc
     */
    public function generate()
    {
        $files = [];

        if ($this->generateModel) {
            $files[] = $this->generateModel();
        }

        if ($this->generateSearchForm) {
            $files[] = $this->generateSearchForm();
        }

        if ($this->generateController) {
            $files[] = $this->generateController();
        }

        if ($this->generateDoc) {
            $files[] = $this->generateDoc();
        }

        return $files;
    }

    /**
     * @param array $params
     * @return CodeFile
     */
    protected function generateModel($params = [])
    {
        return new CodeFile(
            Yii::getAlias('@' . $this->getGenerateModelClass()) . '.php',
            $this->render('model.php', ArrayHelper::merge(['generator' => $this], $params))
        );
    }

    /**
     * @param array $params
     * @return CodeFile
     */
    protected function generateSearchForm($params = [])
    {
        return new CodeFile(
            Yii::getAlias('@' . str_replace('\\', '/', $this->generateSearchFormNamespace)) . '/' . $this->getBaseModelName() . $this->searchFormClassSuffix . '.php',
            $this->render('search-form.php', ArrayHelper::merge(['generator' => $this], $params))
        );
    }

    /**
     * @param array $params
     * @return CodeFile
     */
    protected function generateController($params = [])
    {
        return new CodeFile(
            Yii::getAlias('@' . str_replace('\\', '/', $this->generateControllerNamespace)) . '/' . $this->getBaseModelName() . 'Controller.php',
            $this->render('controller.php', ArrayHelper::merge(['generator' => $this], $params))
        );
    }

    /**
     * @param array $params
     * @return CodeFile
     */
    protected function generateDoc($params = [])
    {
        return new CodeFile(
            Yii::getAlias('@' . str_replace('\\', '/', $this->generateDocFolder)) . '/' . $this->getControllerId() . '.php',
            $this->render('doc.php', ArrayHelper::merge(['generator' => $this], $params))
        );
    }

    /**
     * @return bool|string
     */
    public function getSearchFormName()
    {
        if ($this->generateSearchForm) {
            return $this->getBaseModelName() . $this->searchFormClassSuffix;
        }

        return false;
    }

    /**
     * @return string
     */
    public function getGenerateModelClass()
    {
        if(!$this->_generateModelClass) {
            $this->_generateModelClass = str_replace('\\', '/', $this->generateModelNamespace) . '/' . $this->getBaseModelName();
        }

        return $this->_generateModelClass;
    }

    /**
     * @return mixed
     */
    public function getBaseModelName()
    {
        if(!$this->_baseModelName) {
            $explodedClassName = explode('\\', $this->baseModelClass);
            $this->_baseModelName = end($explodedClassName);
        }

        return $this->_baseModelName;
    }

    /**
     * @return string
     */
    public function getControllerId()
    {
        if(!$this->_controllerId) {
            $this->_controllerId = Inflector::camel2id($this->getBaseModelName());
        }

        return $this->_controllerId;
    }

    /**
     * @return mixed
     */
    public function getBaseModel()
    {
        if(!$this->_baseModel) {
            $modelClass = $this->baseModelClass;
            $this->_baseModel = new $modelClass();
        }

        return $this->_baseModel;
    }

    /**
     * @return mixed
     */
    public function getGeneratedModel()
    {
        try {
            $generateModelClass = '\\' . ltrim(FileHelper::normalizePath($this->getGenerateModelClass(), "\\"), '\\');

            if(!$this->_generatedModel) {
                $this->_generatedModel = new $generateModelClass();
            }

            return $this->_generatedModel;
        } catch (\Exception $ex) {
            return null;
        }
    }

    /**
     * Normalizes [[excludeFields]] into an array of fields.
     * @return array an array fields entered by the user
     */
    public function getExcludeFieldsArray()
    {
        $excludeFields = array_unique(preg_split('/[\s,]+/', $this->excludeFields, -1, PREG_SPLIT_NO_EMPTY));
        sort($excludeFields);

        return $excludeFields;
    }

    /**
     * Normalizes [[extraActions]] into an array of additional actions.
     * @return array an array actions entered by the user
     */
    public function getExtraActions()
    {
        $extraActions = array_unique(preg_split('/[\s,]+/', $this->extraActions, -1, PREG_SPLIT_NO_EMPTY));
        sort($extraActions);

        return $extraActions;
    }
}
