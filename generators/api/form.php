<?php
/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $generator platx\gii\generators\api\Generator */

?>


<?= $form->field($generator, 'baseModelClass') ?>
<?= $form->field($generator, 'generateController')->checkbox(['class' => 'generateController']) ?>

<div class="row generateControllerChild">
    <div class="col-lg-1"></div>
    <div class="col-lg-11"><?= $form->field($generator, 'generateActionIndex')->checkbox() ?></div>
    <div class="col-lg-1"></div>
    <div class="col-lg-11"><?= $form->field($generator, 'generateActionView')->checkbox() ?></div>
    <div class="col-lg-1"></div>
    <div class="col-lg-11"><?= $form->field($generator, 'generateActionCreate')->checkbox() ?></div>
    <div class="col-lg-1"></div>
    <div class="col-lg-11"><?= $form->field($generator, 'generateActionUpdate')->checkbox() ?></div>
    <div class="col-lg-1"></div>
    <div class="col-lg-11"><?= $form->field($generator, 'generateActionDelete')->checkbox() ?></div>
    <div class="col-lg-1"></div>
    <div class="col-lg-11"><?= $form->field($generator, 'extraActions') ?></div>
</div>

<?= $form->field($generator, 'generateModel')->checkbox(['class' => 'generateModel']) ?>
<div class="row generateModelChild">
    <div class="col-lg-1"></div>
    <div class="col-lg-11"><?= $form->field($generator, 'excludeFields') ?></div>
</div>

<?= $form->field($generator, 'generateSearchForm')->checkbox(['class' => 'generateSearchForm']) ?>
<div class="row generateSearchFormChild">
    <div class="col-lg-1"></div>
    <div class="col-lg-11"><?= $form->field($generator, 'searchFormClassSuffix') ?></div>
</div>
<?= $form->field($generator, 'generateDoc')->checkbox() ?>

<?php $this->registerJs("
    var generateController = $('.generateController');
    var generateSearchForm = $('.generateSearchForm');
    var generateModel = $('.generateModel');

    var generateControllerChild = $('.generateControllerChild input');
    var generateSearchFormChild = $('.generateSearchFormChild input');
    var generateModelChild = $('.generateModelChild input');

    checkGenerateController();
    checkGenerateSearchForm();
    checkGenerateModel();

    generateController.on('change', function(){
        checkGenerateController();
    });

    generateSearchForm.on('change', function(){
        checkGenerateSearchForm();
    });


    generateModel.on('change', function(){
        checkGenerateModel();
    });

    function checkGenerateController(){
        if(generateController.is(':checked')) {
            generateControllerChild.prop('disabled', false);
        } else {
            generateControllerChild.prop('disabled', true);
        }
    };

    function checkGenerateSearchForm(){
        if(generateSearchForm.is(':checked')) {
            generateSearchFormChild.prop('disabled', false);
        } else {
            generateSearchFormChild.prop('disabled', true);
        }
    };

    function checkGenerateModel(){
        if(generateModel.is(':checked')) {
            generateModelChild.prop('disabled', false);
        } else {
            generateModelChild.prop('disabled', true);
        }
    };
"); ?>
